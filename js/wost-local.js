
        var containerW = $("#chart").width();
        var containerH = $("#chart").height();
        var padding = {
                top: 0,
                right: 15,
                bottom: 0,
                left: 0
            },
            w = containerW,
            h = containerH,
            r = Math.min(w, h) / 2,
            rotation = 0,
            oldrotation = 0,
            picked = 100000,
            oldpick = [],
            color = d3.scale.category20(); //category20c()
        //randomNumbers = getRandomNumbers();
        var updatePts = 0;

        //http://osric.com/bingo-card-generator/?title=HTML+and+CSS+BINGO!&words=padding%2Cfont-family%2Ccolor%2Cfont-weight%2Cfont-size%2Cbackground-color%2Cnesting%2Cbottom%2Csans-serif%2Cperiod%2Cpound+sign%2C%EF%B9%A4body%EF%B9%A5%2C%EF%B9%A4ul%EF%B9%A5%2C%EF%B9%A4h1%EF%B9%A5%2Cmargin%2C%3C++%3E%2C{+}%2C%EF%B9%A4p%EF%B9%A5%2C%EF%B9%A4!DOCTYPE+html%EF%B9%A5%2C%EF%B9%A4head%EF%B9%A5%2Ccolon%2C%EF%B9%A4style%EF%B9%A5%2C.html%2CHTML%2CCSS%2CJavaScript%2Cborder&freespace=true&freespaceValue=Web+Design+Master&freespaceRandom=false&width=5&height=5&number=35#results

        var data = [{
                "label": "Home",
                "value": 1,
                "question": [{
                        "article": "NEA terminates Spize restaurant's River Valley outlet licences after fatal food poisoning outbreak",
                        "url": "http://localhost/wost/sample/NEA-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Police probing 18 money changers suspected of possessing fake kyat notes after Singaporean couple's arrest in Myanmar",
                        "url": "http://localhost/wost/sample/NEA-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Khaw Boon Wan tells Malaysian ships to back off as Singapore expands port limits",
                        "url": "http://localhost/wost/sample/NEA-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Home
            {
                "label": "Sports",
                "value": 1,
                "question": [{
                        "article": "Athletics: Standard Chartered Singapore Marathon targets major strides",
                        "url": "http://localhost/wost/sample/Athletics-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Football: Chelsea boss Sarri demands a 'big reaction' after shock defeat by Wolves",
                        "url": "http://localhost/wost/sample/Athletics-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Basketball: Derrick Favors, Utah Jazz rout reeling Houston Rockets",
                        "url": "http://localhost/wost/sample/Athletics-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Sports
            {
                "label": "Business",
                "value": 1,
                "question": [{
                        "article": "SGX daily average value of securities in November down 21% year on year but derivatives volume up 9%",
                        "url": "http://localhost/wost/sample/SGX-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Noble may seek court-appointed administration in Britain after Singapore authorities block relisting: Board reiterates",
                        "url": "http://localhost/wost/sample/SGX-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Sheng Siong launches 'recycling' cash withdrawal machines at their supermarkets",
                        "url": "http://localhost/wost/sample/SGX-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Business
            {
                "label": "Tech",
                "value": 1,
                "question": [{
                        "article": "StarHub overhauls mobile plans to defend market share as TPG Telecom's launch nears",
                        "url": "http://localhost/wost/sample/StarHub-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Verizon, Samsung to release 5G smartphones in US in 2019",
                        "url": "http://localhost/wost/sample/StarHub-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Apple picks up apps created by SST students",
                        "url": "http://localhost/wost/sample/StarHub-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Tech
            {
                "label": "Polities",
                "value": 1,
                "question": [{
                        "article": "Defence Minister Ng Eng Hen warns Malaysian vessels to stay out of Singapore waters",
                        "url": "http://localhost/wost/sample/Defence-Minister-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Foreign Minister Vivian Balakrishnan urges Malaysia to cease intrusions into Singapore waters",
                        "url": "http://localhost/wost/sample/Defence-Minister-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Leong Sze Hian rejects allegations that he shared article maliciously and to damage PM Lee",
                        "url": "http://localhost/wost/sample/Defence-Minister-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Polities
            {
                "label": "Asia",
                "value": 1,
                "question": [{
                        "article": "Singaporeans advised to defer non-essential travel to Kuala Lumpur over weekend rallies",
                        "url": "http://localhost/wost/sample/Singaporeans-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Arrest shakes Huawei as global scepticism of its business grows",
                        "url": "http://localhost/wost/sample/Singaporeans-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Guangzhou Knowledge City can be springboard for Singapore firms into the Greater Bay Area: Ong Ye Kung",
                        "url": "http://localhost/wost/sample/Singaporeans-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //Asia
            {
                "label": "World",
                "value": 1,
                "question": [{
                        "article": "EU's tech commissioner says Europe should be worried about Huawei",
                        "url": "http://localhost/wost/sample/EU-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Fearing 'Act IV' of unrest, France to close Eiffel Tower, Louvre, over weekend",
                        "url": "http://localhost/wost/sample/EU-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Developing nations say swifter climate action depends on cash to pay for it",
                        "url": "http://localhost/wost/sample/EU-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            }, //World
            {
                "label": "Lifestyle",
                "value": 1,
                "question": [{
                        "article": "Award-winning Kampung Admiralty project attracts international attention",
                        "url": "http://localhost/wost/sample/Award-winning-The-Straits-Times.htm",
                        "points": 100
                    },
                    {
                        "article": "More Singapore fashion entrepreneurs are jumping on the athleisure trend",
                        "url": "http://localhost/wost/sample/Award-winning-The-Straits-Times.htm",
                        "points": 200
                    },
                    {
                        "article": "Singer and guitarist Zul Sutan: A lion of a musician",
                        "url": "http://localhost/wost/sample/Award-winning-The-Straits-Times.htm",
                        "points": 300
                    },
                ]
            }, //Lifestyle
            {
                "label": "Food",
                "value": 1,
                "question": [{
                        "article": "Food Picks: Aromatic prawn ramen, roast Christmas goose, Omakase Burger gets better bun, weekend dimsum brunch at Golden Peony",
                        "url": "http://localhost/wost/sample/Food-Picks-TheStraitsTimes.htm",
                        "points": 100
                    },
                    {
                        "article": "Restaurant Review: Christmas dishes with a difference at Botanico",
                        "url": "http://localhost/wost/sample/Food-Picks-TheStraitsTimes.htm",
                        "points": 200
                    },
                    {
                        "article": "Cheap & Good: Comforting puffs at Soon Soon Huat Curry Puff in East Coast",
                        "url": "http://localhost/wost/sample/Food-Picks-TheStraitsTimes.htm",
                        "points": 300
                    },
                ]
            } //Food
        ];


        var svg = d3.select('#chart')
            .append("svg")
            .data([data])
            .attr("width", w + padding.left + padding.right)
            .attr("height", h + padding.top + padding.bottom);

        var container = svg.append("g")
            .attr("class", "chartholder")
            .attr("transform", "translate(" + (w / 2 + padding.left) + "," + (h / 2 + padding.top) + ")");

        var vis = container
            .append("g");

        var pie = d3.layout.pie().sort(null).value(function (d) {
            return 1;
        });

        // declare an arc generator function
        var arc = d3.svg.arc().outerRadius(r);

        // select paths, use arc generator to draw
        var arcs = vis.selectAll("g.slice")
            .data(pie)
            .enter()
            .append("g")
            .attr("class", "slice");


        arcs.append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", function (d) {
                return arc(d);
            });

        // add the text
        arcs.append("text").attr("transform", function (d) {
                d.innerRadius = 0;
                d.outerRadius = r;
                d.angle = (d.startAngle + d.endAngle) / 2;
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")translate(" + (d.outerRadius - 20) + ")";
            })
            .attr("text-anchor", "end")
            .text(function (d, i) {
                return data[i].label;
            });

        container.on("click", spin);


        function spin(d) {

            container.on("click", null);

            //all slices have been seen, all done
            console.log("OldPick: " + oldpick.length, "Data length: " + data.length);
            if (oldpick.length == data.length) {
                console.log("done");
                container.on("click", null);
                return;
            }

            var ps = 360 / data.length,
                pieslice = Math.round(1440 / data.length),
                rng = Math.floor((Math.random() * 1440) + 360);

            rotation = (Math.round(rng / ps) * ps);

            picked = Math.round(data.length - (rotation % 360) / ps);
            picked = picked >= data.length ? (picked % data.length) : picked;


            if (oldpick.indexOf(picked) !== -1) {
                d3.select(this).call(spin);
                return;
            } else {
                oldpick.push(picked);
            }

            rotation += 90 - Math.round(ps / 2);

            vis.transition()
                .duration(3000)
                .attrTween("transform", rotTween)
                .each("end", function () {

                   
                    //mark question as seen
                    d3.select(".slice:nth-child(" + (picked + 1) + ") path");

                    //populate get list of news
                    $('.selected-question h1').text(data[picked].label);
                    $('.selected-articles h4').text(data[picked].label);
                    
                    var newslist = "";
                    for (i = 0; i < data[picked].question.length; i++) {
                        newslist += "<li class='news-link' data-src='" + data[picked].question[i].url + "' data-pts='"  +  data[picked].question[i].points + "'>" + data[picked].question[i]
                            .article + "  <sup> --" + data[picked].question[i].points + "</sup></li>";
                    }
                    d3.select("#question ul")
                        .html(newslist);

                    oldrotation = rotation;

                    container.on("click", spin);

                     $('.articles-panel').fadeIn(200);
                    $('.selected-question').fadeIn(200).delay(2000).fadeOut(300);
                    $('.selected-articles').delay(2500).fadeIn(200);
                });
        }

        //make arrow
        svg.append("g")
            .attr("transform", "translate(" + (w + padding.left + padding.right) + "," + ((h / 2) + padding.top) + ")")
            .append("path")
            .attr("d", "M-" + (r * .15) + ",0L0," + (r * .05) + "L0,-" + (r * .05) + "Z")
            .style({
                "fill": "#0c2b57"
            });

        //draw spin circle
        container.append("circle")
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", 60)
            .style({
                "fill": "white",
                "cursor": "pointer"
            });

        //spin text
        container.append("text")
            .attr("x", 0)
            .attr("y", 15)
            .attr("text-anchor", "middle")
            .text("SPIN")
            .style({
                "font-weight": "bold",
                "font-size": "30px"
            });


        function rotTween(to) {
            var i = d3.interpolate(oldrotation % 360, rotation);
            return function (t) {
                return "rotate(" + i(t) + ")";
            };
        }


        function getRandomNumbers() {
            var array = new Uint16Array(1000);
            var scale = d3.scale.linear().range([360, 1440]).domain([0, 100000]);

            if (window.hasOwnProperty("crypto") && typeof window.crypto.getRandomValues === "function") {
                window.crypto.getRandomValues(array);
                console.log("works");
            } else {
                //no support for crypto, get crappy random numbers
                for (var i = 0; i < 1000; i++) {
                    array[i] = Math.floor(Math.random() * 100000) + 1;
                }
            }

            return array;
        }

        function addPoints(pts){
           var ttlPts =  parseInt($('.points-border h2').text());
            
           var addOnNumber = parseInt(pts);

           ttlPts += addOnNumber;
           $('.points-border h2').text(ttlPts);
            console.log(ttlPts);
            console.log(pts);
        
        }
        $(document).ready(function(){
            $('.closeBtn').click(function(){
                $('.articles-panel').fadeOut(300);
                // $('.selected-question').fadeIn(200).delay(2000).fadeOut(300);
                $('.selected-articles').fadeOut(200);
                
            });

            $('#news-close').click(function(){

                $('.news-frame').fadeOut(300);
            });

            $('#question').on('click', 'li', function (event) {
                
                var nlink = $(this).attr('data-src');
                var nPts = $(this).attr('data-pts');

                addPoints(nPts);
                $('.news-container').html('<object data="' + nlink + '"/>');
                $(".news-frame").fadeIn(300);
            });
        }); 